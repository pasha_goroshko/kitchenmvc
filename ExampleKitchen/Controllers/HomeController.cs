﻿using ExampleKitchen.DomainModel;
using ExampleKitchen.Helpers;
using ExampleKitchen.Models;
using ExampleKitchen.WorkExcel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExampleKitchen.Controllers
{
    [Authorize(Users = @"01TMP03\User,01TMP03\qwe")]
    public class HomeController : Controller
    {
        // GET: Home
        [Authorize]
        public ActionResult Index()
        {
            var nowDate = DateTime.Now.Date;
            var menu = new ClientContext().Menu.Where(x => x.DateMenu >= nowDate).FirstOrDefault();

            if (menu == null)
                menu = new ClientContext().Menu.ToList().LastOrDefault();


            var res = new Mapper().MenuMapper(menu);

                ViewData.Model = res;
                return View();
        }

        [Authorize(Roles = "Administrator,User")]
        public ActionResult LoadFile()
        {
            return View("~/Views/File/LoadFile.cshtml");
        }

        [Authorize]
        public ActionResult PersonalCabinet()
        {
            return View("~/Views/Client/Cabinet.cshtml");
        }

        [HttpPost]
        public ActionResult Index(MenuModel Menu)
        {
            var res = new ClientContext();

            var menu = new Mapper().MenuModelMapper(Menu);
            Order order = new Order();
            order.DateOrder = DateTime.Now;
            order.MenuId = menu.MenuId;

            foreach (var item in Menu._Food)
            {
                foreach (var Item2 in item.Dishes)
                {
                    if (Item2.IsOrder)
                        order.OrderDish_.Add(new OrderDish
                        {
                            Dish_ = res.Dishes.Where(x => x.ID == Item2.ID).FirstOrDefault()
                        });
                }
            }

            res.Clients.Where(x => x.Name == User.Identity.Name).FirstOrDefault().Order_.Add(order);

            res.SaveChanges();

           

            return RedirectToAction("Cabinet", "Client");
      
        }
    }
}