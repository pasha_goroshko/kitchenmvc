﻿using ExampleKitchen.DomainModel;
using ExampleKitchen.Helpers;
using ExampleKitchen.Models;
using ExampleKitchen.WorkExcel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExampleKitchen.Controllers
{
    [Authorize]
    public class ClientController : Controller
    {
        // GET: Client
        [Authorize]
        public ActionResult Cabinet()
        {
            var res = new ClientContext();
            var client = res.Clients.Where(x => x.Name == User.Identity.Name).FirstOrDefault();

            if (client != null)
            {
                if (client.Order_.Count != 0)
                    ViewData.Model = client.Order_
                        .Select(x => new Mapper().OrderMapper(x))
                        .ToList();

                return View();
            }
            else
                return View("~/Views/Client/LogIn.cshtml");
        }

        public ActionResult Save()
        {
            new MenuDish().GetAllOrders(System.Configuration.ConfigurationManager.AppSettings["FilePath"].Replace(@"\", "/"));

            return View();
        }

        [Authorize]
        public ActionResult Delete(int id)
        {
            var res = new ClientContext();

            res.OrderDishes.RemoveRange(res.Orders.Where(x => x.Id == id).FirstOrDefault().OrderDish_);

            res.Orders.Remove(res.Orders.Where(x => x.Id == id).FirstOrDefault());

            res.SaveChanges();

            return Redirect(Request.UrlReferrer.ToString());
        }

        [HttpPost]
        public ActionResult Cabinet(ClientModel client)
        {

            try
            {
                var res = new ClientContext();
                res.Clients.Add(new Client
                {
                    Email = client.Email,
                    Name = User.Identity.Name,
                    UserName = client.UserName
                });

                res.SaveChanges();
            }
            catch(Exception ex)
            {

            }

            return View();
        }
    }
}