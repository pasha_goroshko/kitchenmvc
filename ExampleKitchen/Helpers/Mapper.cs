﻿using ExampleKitchen.DomainModel;
using ExampleKitchen.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExampleKitchen.Helpers
{
    public class Mapper
    {
        public OrderDishModel OrderDishMapper(OrderDish orderDish)
        {
            return new OrderDishModel
            {
                Dish_ = DishMapper(orderDish.Dish_),
                Id = orderDish.Id
            };
        }

        public OrderModel OrderMapper(Order order)
        {
            return new OrderModel
            {
                ClientId = order.ClientId,
                Id = order.Id,
                DateOrder = order.DateOrder,
                OrderDish_ = order.OrderDish_.Select(x => OrderDishMapper(x)).ToList(),
                MenuId = order.MenuId,
                PriceOrder = new Purchase().GetTotalPrice(null, order)
            };
        }

        public MenuModel MenuMapper(Menu menu)
        {
            if (menu != null)
            {
                return new MenuModel
                {
                    DateMenu = menu.DateMenu,
                    Info = menu.Info,
                    IsAvailable = menu.IsAvailable,
                    MenuId = menu.MenuId,
                    _Food = menu._Food.Select(x => FoodMapper(x)).ToList()
                };
            }
            else
                return null;
        }

        public FoodModel FoodMapper(Food food)
        {
            return new FoodModel
            {
                ID = food.ID,
                MenuId = food.MenuId,
                Type = (TypeofDishModel)(int)food.Type,
                Dishes = food.Dishes.Select(x => DishMapper(x)).ToList()
            };
        }

        public DishModel DishMapper(Dish dish)
        {
            return new DishModel
            {
                FoodId = dish.FoodId,
                ID = dish.ID,
                IsOrder = dish.IsOrder,
                Name = dish.Name,
                Price = dish.Price,
                Weight = dish.Weight
            };
        }
        public Menu MenuModelMapper(MenuModel menu)
        {
            return new Menu
            {
                DateMenu = menu.DateMenu,
                Info = menu.Info,
                MenuId = menu.MenuId,
                _Food = menu._Food.Select(x => FoodModelMapper(x)).ToList()
            };
        }

        public Food FoodModelMapper(FoodModel food)
        {
            return new Food
            {
                ID = food.ID,
                MenuId = food.MenuId,
                Type = (TypeofDish)(int)food.Type,
                Dishes = food.Dishes.Select(x => DishModelMapper(x)).ToList()
            };
        }

        public Dish DishModelMapper(DishModel dish)
        {
            return new Dish
            {
                FoodId = dish.FoodId,
                ID = dish.ID,
                IsOrder = dish.IsOrder,
                Name = dish.Name,
                Price = dish.Price,
                Weight = dish.Weight,
                IndexDish = dish.IndexDish
            };
        }
    }
}