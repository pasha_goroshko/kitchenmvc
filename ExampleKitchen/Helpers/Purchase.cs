﻿using ExampleKitchen.DomainModel;
using ExampleKitchen.Models;
using ExampleKitchen.WorkExcel;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace ExampleKitchen.Helpers
{
    public class Purchase
    {
        public bool CheckFullComplex(Menu menu, Order order)
        {
            if (menu == null)
            {
                var count = order.OrderDish_.GroupBy(x => x.Dish_.FoodId).Select(x => x.First()).Count();

                if (order.OrderDish_.Count != count)
                    return false;
                else
                    return true;

            } 

            foreach (var food in menu._Food)
            {
                var dishes = food.Dishes.Where(x => x.IsOrder).ToList();

                if (dishes == null)
                    return false;
                if (dishes.Count != 1)
                    return false;
            }

            return true;
        }

        public double GetTotalPrice(Menu menu , Order ord)
        {
            var totalPrice = 0.0;

            if (!CheckFullComplex(menu, ord))
            {
                if (menu != null)
                {
                    foreach (var food in menu._Food)
                    {
                        var dishes = food.Dishes.Where(x => x.IsOrder).ToList();

                        foreach (var dish in dishes)
                        {
                            totalPrice += dish.Price;
                        }
                    }
                    return totalPrice;
                }
                else
                   return PriceOrder(ord);
            }
            else
                return double.Parse(WebConfigurationManager.AppSettings["PriceFullComplex"]);
        }

        private double PriceOrder(Order order)
        {
            var count = 0.0;
            foreach (var dishOrd in order.OrderDish_)
            {
                count += dishOrd.Dish_.Price;
            }

            return count;
        }

        public void SaveInDB(DateTime dt,string path)
        {
            var res = new MenuDish().GetMenu(path);

            var context = new ClientContext();

            var food = new List<Food>();
            foreach (var item in res._Food)
            {
                var list = new List<Dish>();
                foreach (var item2 in item.Dishes)
                {
                    list.Add(new Dish
                    {
                        Name = item2.Name,
                        Price = item2.Price,
                        Weight = item2.Weight,
                    });
                }

                food.Add(new Food
                {
                    Type = (TypeofDish)item.Type,
                    Dishes = list
                });
            }

            context.Menu.Add(new Menu
            {
                DateMenu = dt,
                Info = res.Info,
                _Food = food
            });

            context.SaveChanges();
        }
    }
}