﻿using ExampleKitchen.DomainModel;
using ExampleKitchen.Helpers;
using ExampleKitchen.Models;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web.Configuration;

namespace ExampleKitchen.WorkExcel
{
    public class MenuDish
    {
        string path;

        public Menu GetMenu(string _path)
        {
            if (_path == null)
                path = WebConfigurationManager.AppSettings["FilePath"].Replace(@"\", "/");
            else
                path = _path;

            return ParseDataFromExcel();
        }

        private Menu ParseDataFromExcel()
        {
            int id = 0;
            var menu = new Menu();

            var package = new ExcelPackage(new FileInfo(path));

            ExcelWorksheet workSheet = package.Workbook.Worksheets[1];

            menu.Info = workSheet.Cells[4, 4].Value.ToString();

            for (int i = workSheet.Dimension.Start.Row + 5; i <= workSheet.Dimension.End.Row; i++)
            {
                // Считываем тип блюда
                if (workSheet.Cells[i, 1].Value!=null )
                {
                    menu._Food.Add(new Food { Type = GetTypeDish(workSheet.Cells[i, 1].Value.ToString()) });
                }
                // Проверяем на достигнут ли конец листа.
                if (workSheet.Cells[i, 2].Value == null)
                    break;

                // Добавляем еду.
                menu._Food[menu._Food.Count - 1].Dishes.Add(new Dish
                {
                    IndexDish = id++,
                    Name = workSheet.Cells[i, 4].Value.ToString(),
                    Price = double.Parse(workSheet.Cells[i, 3].Value.ToString()),
                    Weight = double.Parse(workSheet.Cells[i, 2].Value.ToString())
                });
            }

            return menu;
        }

        public string GetAllOrders(string _path)
        {
            var package = new ExcelPackage(new FileInfo(_path));

            ExcelWorksheet workSheet = package.Workbook.Worksheets[1];

            workSheet.Cells[4, 4].Value = System.Configuration.ConfigurationManager.AppSettings["Info"];


            var context = new ClientContext();
            var orders = context.Orders.ToList();

            int index = 5;

            for (int i = 0; i < orders.Count; i++)
            {
                // Записываем имя.
                workSheet.Cells[4, index].Value = orders[i].Client_.UserName;
                // Проверяем на полный комплекс.
                workSheet.Cells[5, index].Value = new Purchase().CheckFullComplex(null, orders[i]) == true ? "1" : "";

                //Записываем все меню.
                foreach (var item in orders[i].OrderDish_)
                {
                    workSheet.Cells[++item.Dish_.IndexDish, index].Value = 1;
                }

                ++index;
            }


            package.Save();
            return null;
        }

        /// <summary>
        /// Возвращет тип enum TypeofDish блюда.
        /// </summary>
        /// <param name="_type">Тип в строковом представлении.</param>
        /// <returns></returns>
        private TypeofDish GetTypeDish(string _type)
        {
            switch(_type)
            {
                case "Салат":return TypeofDish.Salad;
                case "Первое блюдо": return TypeofDish.FirstCours;
                case "Второе блюдо": return TypeofDish.SecondCours;
                case "Гарнир":return TypeofDish.Garnish;
                default:return TypeofDish.Unknown;
            }
        }
    }
}