﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ExampleKitchen.DomainModel
{
    public class Order
    {
        public int Id { get; set; }
        public DateTime DateOrder { get; set; }

        public int ClientId { get; set; }

        public virtual Client Client_ { get; set; }

        public virtual List<OrderDish> OrderDish_ { get; set; }

        public int MenuId { get; set; }

        public Order()
        {
            OrderDish_ = new List<OrderDish>();
        }
    }
}