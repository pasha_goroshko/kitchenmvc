﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExampleKitchen.DomainModel
{
    public class Menu
    {
        public int MenuId { get; set; }
        public string Info { get; set; }
        public DateTime DateMenu { get; set; }
        public bool IsAvailable { get; set; }
        public virtual List<Food> _Food { get; set; }

        public Menu()
        {
            _Food = new List<Food>();
        }
    }
}