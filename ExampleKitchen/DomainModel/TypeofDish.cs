﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExampleKitchen.DomainModel
{
        public enum TypeofDish : int
        {
            Unknown = 0,
            Salad = 1,
            FirstCours = 2,
            SecondCours = 3,
            Garnish = 4
        }
}