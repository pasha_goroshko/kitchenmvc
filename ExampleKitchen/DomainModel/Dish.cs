﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExampleKitchen.DomainModel
{
    public class Dish
    {
        public int ID { get; set; }
        public int FoodId { get; set; }
        public int IndexDish { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public double Weight { get; set; }
        public bool IsOrder { get; set; }
    }
}