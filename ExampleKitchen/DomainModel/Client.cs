﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ExampleKitchen.DomainModel
{
    public class Client
    {
        public int ClientID { get; set; }

        public string Name { get; set; }
      
        public string UserName { get; set; }

        public string Email { get; set; }

        public virtual List<Order> Order_ { get; set; }

        public Client()
        {
            Order_ = new List<Order>();
        }
    }
}