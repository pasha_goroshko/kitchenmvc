﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExampleKitchen.DomainModel
{
    public class Food
    {
        public int ID { get; set; }
        public int MenuId { get; set; }
        public TypeofDish Type { get; set; }
        public virtual List<Dish> Dishes { get; set; }

        public Food()
        {
            Dishes = new List<Dish>();
        }
    }
}