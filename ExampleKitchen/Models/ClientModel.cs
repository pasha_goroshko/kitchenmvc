﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ExampleKitchen.Models
{
    public class ClientModel
    {
        public int ClientID { get; set; }
        
        public string Name { get; set; }
        [Required]
        [Display(Name = "Имя Фамилия")]
        [StringLength(50,MinimumLength = 4,ErrorMessage = "Вы ввели недопустимое значение!")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Email")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Некорректный адрес")]
        public string Email { get; set; }

        public virtual List<OrderModel> Order_ { get; set; }

        public ClientModel()
        {
            Order_ = new List<OrderModel>();
        }
    }
}