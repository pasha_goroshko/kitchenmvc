﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;

namespace ExampleKitchen.Models
{
    public enum TypeofDishModel : int
    {
        [Display(Name = "Неизвестно")]
        Unknown = 0,
        [Display(Name = "Салат")]
        Salad = 1,
        [Display(Name = "Первое блюдо")]
        FirstCours = 2,
        [Display(Name = "Второе блюдо")]
        SecondCours = 3,
        [Display(Name = "Гарнир")]
        Garnish = 4
    }
}