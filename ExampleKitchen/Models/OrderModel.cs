﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ExampleKitchen.Models
{
    public class OrderModel
    {
        public int Id { get; set; }
        [Display(Name = "Дата и время заказа")]
        public DateTime DateOrder { get; set; }

        public int ClientId { get; set; }

        [Display(Name = "Цена, BYN")]
        public double PriceOrder { get; set; }

        public virtual List<OrderDishModel> OrderDish_ { get; set; }

        public int MenuId { get; set; }

        public OrderModel()
        {
            OrderDish_ = new List<OrderDishModel>();
        }

        public virtual ClientModel Client_ { get; set; }
    }
}