﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;


namespace ServiceKitchen.Model
{
    class Sender
    {
        public string UserMail { get; set; }
        public string Password { get; set; }

        public int PortSMTP { get; set; }
        public bool EnableSSlSMTP { get; set; }
        public string HostSMTP { get; set; }

        public int PortIMAP { get; set; }
        public bool EnableSSlIMAP { get; set; }
        public string HostIMAP { get; set; }


        public Addresses AddressesUser { get; set; }

        public Sender()
        {
            AddressesUser = new Addresses();

            UserMail = ConfigurationSettings.AppSettings["UserName"];
            Password = ConfigurationSettings.AppSettings["Password"];

            PortSMTP = int.Parse(ConfigurationSettings.AppSettings["PortSMTP"]);
            EnableSSlSMTP = bool.Parse(ConfigurationSettings.AppSettings["EnableSSLSMTP"]);
            HostSMTP = ConfigurationSettings.AppSettings["HostSMTP"];

            PortIMAP = int.Parse(ConfigurationSettings.AppSettings["PortIMAP"]);
            EnableSSlIMAP = bool.Parse(ConfigurationSettings.AppSettings["EnableSSLIMAP"]);
            HostIMAP = ConfigurationSettings.AppSettings["HostIMAP"];
        }
    }
}
