﻿using ServiceKitchen.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace ServiceKitchen
{
    public partial class Service1 : ServiceBase
    {
        Timer timer;
        DateTime timeSend = DateTime.Parse(ConfigurationSettings.AppSettings["TimeSend"]);
        DateTime timeCheck = DateTime.Parse(ConfigurationSettings.AppSettings["TimeCheckTo"]);

        public Service1()
        {
            InitializeComponent();
            new Mail().SendFile("");
        }

        protected override void OnStart(string[] args)
        {
            timer = new Timer();
            if (DateTime.Now < timeCheck && DateTime.Now > timeCheck.AddHours(-2))
                timer.Interval = 60000 * 5;
            else
                timer.Interval = timeCheck.AddHours(-2).Subtract(DateTime.Now).TotalMilliseconds;
            timer.Elapsed += Timer_Elapsed;
            timer.Enabled = true;

        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (!(new Mail().GetAttachment()))
            {
                timer.Elapsed -= Timer_Elapsed;
                timer.Elapsed += Timer_Elapsed1;
                timer.Interval = timeSend.Subtract(DateTime.Now).TotalMilliseconds;
            }
        }

        private void Timer_Elapsed1(object sender, ElapsedEventArgs e)
        {
            new Mail().SendFile(ConfigurationSettings.AppSettings["PathFile"]);
            timer.Elapsed += Timer_Elapsed;
            timer.Elapsed -= Timer_Elapsed1;
            timer.Interval = timeCheck.AddHours(-2).Subtract(DateTime.Now).TotalMilliseconds;
        }

        protected override void OnStop()
        {

        }
    }
}
