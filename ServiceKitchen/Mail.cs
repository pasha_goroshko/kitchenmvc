﻿using ExampleKitchen.DomainModel;
using Limilabs.Client.IMAP;
using Limilabs.Mail;
using Limilabs.Mail.MIME;
using ServiceKitchen.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ServiceKitchen
{
    class Mail
    {
        Sender user = new Sender();

        public bool GetAttachment()
        {
            string path = null;
            using (Imap imap = new Imap())
            {
                imap.Connect(user.HostIMAP, user.PortIMAP, user.EnableSSlIMAP);
                imap.UseBestLogin(user.UserMail, user.Password);

                imap.SelectInbox();

                List<long> uids = imap.Search(Flag.All);

                foreach (long uid in uids.OrderByDescending(x => x))
                {
                    IMail email = new MailBuilder()
                        .CreateFromEml(imap.GetMessageByUID(uid));

                    //Проверяем на совпадение адрессов и дат
                    if (email.Date.Value.ToShortDateString() == DateTime.Now.ToShortDateString())
                    {
                        if (email.From[0].Address == user.AddressesUser.UserEmail)
                        {
                            foreach (MimeData mime in email.Attachments)
                            {
                                mime.Save(mime.SafeFileName);
                                path = Path.GetFullPath(mime.SafeFileName);

                                //Сохраняем путь в файл конфигураций
                                ConfigurationSettings.AppSettings["PathFile"] = path;
                                //Сохраняем файл в базу данных.
                                new ExampleKitchen.Helpers.Purchase().SaveInDB(DateTime.Now, path);

                                return true;
                            }
                            break;
                        }
                    }
                    else
                        break;
                }
                imap.Close();
            }

            return false;
        }

        public void SendFile(string path)
        {
            ClientContext context = new ClientContext();
            
            SmtpClient client = new SmtpClient(user.HostSMTP, user.PortSMTP);
            client.EnableSsl = user.EnableSSlSMTP;
            client.Credentials = new NetworkCredential(user.UserMail, user.Password);

            MailMessage message = new MailMessage(user.UserMail, user.AddressesUser.UserEmail);
            message.Attachments.Add(new Attachment(path));

            client.Send(message);
        }
    }
}
